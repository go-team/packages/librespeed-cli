module github.com/librespeed/speedtest-cli

go 1.14

require (
	github.com/briandowns/spinner v1.23.0
	github.com/fatih/color v1.15.0 // indirect
	github.com/go-ping/ping v1.1.0
	github.com/gocarina/gocsv v0.0.0-20230406101422-6445c2b15027
	github.com/google/uuid v1.3.0 // indirect
	github.com/mattn/go-isatty v0.0.18 // indirect
	github.com/sirupsen/logrus v1.9.0
	github.com/urfave/cli/v2 v2.25.1
	golang.org/x/net v0.9.0 // indirect
	golang.org/x/sys v0.7.0
)
